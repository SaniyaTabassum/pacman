def square(x, y):
    "Draw square using path at (x,y)."
    path.up()
    path.goto(x, y)
    path.down()
    path
    .begin_fill()

    for count in range(4):
        path.forward(20)
        path.left(90)

    path.end_fill()
