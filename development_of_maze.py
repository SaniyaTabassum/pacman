Python 3.10.0 (tags/v3.10.0:b494f59, Oct  4 2021, 19:00:18) [MSC v.1929 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
import pygame
import time
import random

# set up pygame window
WIDTH = 500
HEIGHT = 600
FPS = 30

# Define colours
WHITE = (255, 255, 225)
GREEN = (0, 225, 0,)
BLUE = (0, 0, 255)
YELLOW = (225, 225 ,0)

# initalize Pygame
pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Python Maze Generator")
clock = pygame.time.Clock()

# setup maze variables
x = 0
y = 0
w = 20
grid = []
visited = []
stack = []
solution = {}


# build the grid
def build_grid(x, y, w):
    for i in range(1,21):
        x = 20
        y = y + 20
        for j in range(1, 21 ):
            pygame.draw.line(screen, WHITE, [x, y], [x + w, y])
            pygame.draw.line(screen, WHITE, [x + w, y], [x + w, y + w])
            pygame.draw.line(screen, WHITE, [x + w, y + w], [x , y + w])
            pygame.draw.line(screen, WHITE, [x, y + w], [x , y])
            grid.append((x,y))
            x = x + 20


def push_up(x, y):
    pygame.draw.rect(screen, BLUE, (x = 1, y - w + 1, 19, 39), 0)
    pygame.display.update()


def push_down(x, y):
    pygame.draw.rect(screen, BLUE, (x + 1, y + 1, 19, 39), 0)
    pygame.display.update()


def push_left(x, y):
    pygame.draw.rect(screen, BLUE, (x - w +1, y + 1, 19, 39), 0)
    pygame.display.update()


def push_right(x, y):
    pygame.draw.rect(screen, BLUE, (x + 1, y + 1, 19, 39), 0)
    pygame.display.update()
