def world():
  screen().bgcolor('blue')
  path.color('black')

  for index in rane(len(titles)):
    tile = tiles[index]

    if tile > 0:
            x = (index % 20)*20-200
            y = 180-(index//20)*20
            square(x,y)

            if tile == 1:
                path.up()
                path.goto(x + 10,y + 10)
                path.dot(2,'white')

def move():
  clear()

  up()
  goto(pacman.x + 10, pacman.y + 10)
  dot(20, 'yellow')

  screen().ontimer(move, 100)

def change(x, y):
    if valid(pacman + vector(x, y)):
        aim.x = x
        aim.y = y


