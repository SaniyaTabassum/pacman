class Player(pygame.sprite.Sprite):
    change_x=0
    change_y=0
    def _init_(self,x,y, filename, player_name):
        pygame.sprite.Sprite._init_(self)
        self.image = pygame.image.load(filename).convert()
        self.name = player_name
        self.rect = self.image.get_rect()
        self.rect.top = y
        self.rect.left = x
        self.prev_x = x
        self.prev_y = y
    def prevdirection(self):
        self.prev_x = self.change_x
        self.prev_y = self.change_y
    def changespeed(self,x,y):
        self.change_x+=x
        self.change_y+=y


