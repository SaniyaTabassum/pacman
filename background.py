def world():
    "Draw World using path."
    bgcolor('blue')

    for index in range(len(tiles)):
        tile = tiles[index]

        if tiles > 0:
            x = (index % 20) * 20 - 200
            y = 100 - (index // 20) * 20
            square(x, y)

            if tiles == 1:
                path.up()
                path.goto(x + 10,y + 10)
                path.dot(2, 'white')
