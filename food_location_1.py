cherry_locations =  [[x, y] for x in some_rows for y in some_columns]
  cherry_location_ids = np.random.choice([x for x in range(81)], 4)
  cherry_location = [cherry_locations[x] for x in cherry_location_ids]
  for row in range(19):
      for column in range(19):
          if (row == 7 or row == 8) and (column == 8 or column == 9 or column == 10):
              continue
          elif [row, column] in cherry_location:
            block = Block(red, 8, 8)
            block.rect.x = (30*column+6)+26
            block.rect.y = (30*row+6)+26

            b_collide = pygame.sprite.spritecollide(block, wall_list, False)
            p_collide = pygame.sprite.spritecollide(block, pacman_collide, False)
            if b_collide:
              continue
            elif p_collide:
              continue
            else:
              # Add the block to the list of objects
              block_list_cherry.add(block)
              all_sprites_list.add(block)

          else:
            block = Block(yellow, 4, 4)
