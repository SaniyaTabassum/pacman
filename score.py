score = 0
  done = False
  i = 0
  while done == False:
      for event in pygame.event.get():
          if event.type == pygame.QUIT:
              done=True
          if event.type == pygame.KEYDOWN:
              if event.key == pygame.K_LEFT:
                  Pacman.changespeed(-30,0)
              if event.key == pygame.K_RIGHT:
                  Pacman.changespeed(30,0)
              if event.key == pygame.K_UP:
                  Pacman.changespeed(0,-30)
              if event.key == pygame.K_DOWN:
                  Pacman.changespeed(0,30)
          if event.type == pygame.KEYUP:
              if event.key == pygame.K_LEFT:
                  Pacman.changespeed(30,0)
              if event.key == pygame.K_RIGHT:
                  Pacman.changespeed(-30,0)
              if event.key == pygame.K_UP:
                  Pacman.changespeed(0,30)
              if event.key == pygame.K_DOWN:
                  Pacman.changespeed(0,-30)
      Pacman.update(wall_list,gate)      
      if not pinky_killed:
        returned = Pinky.changespeed(Pinky_directions,False,p_turn,p_steps,pl)
        p_turn = returned[0]
        p_steps = returned[1]
        Pinky.changespeed(Pinky_directions,False,p_turn,p_steps,pl)
        Pinky.update(wall_list,False)
      if not blinky_killed:
        returned = Blinky.changespeed(Blinky_directions,False,b_turn,b_steps,bl)
        b_turn = returned[0]
        b_steps = returned[1]
        Blinky.changespeed(Blinky_directions,False,b_turn,b_steps,bl)
        Blinky.update(wall_list,False)
      if not inky_killed:
        returned = Inky.changespeed(Inky_directions,False,i_turn,i_steps,il)
        i_turn = returned[0]
        i_steps = returned[1]
        Inky.changespeed(Inky_directions,False,i_turn,i_steps,il)
        Inky.update(wall_list,False)
      if not clyde_killed:
        returned = Clyde.changespeed(Clyde_directions,"clyde",c_turn,c_steps,cl)
        c_turn = returned[0]
        c_steps = returned[1]
        Clyde.changespeed(Clyde_directions,"clyde",c_turn,c_steps,cl)
        Clyde.update(wall_list,False)
      blocks_hit_list = pygame.sprite.spritecollide(Pacman, block_list, True)
      blocks_hit_list_cherry = pygame.sprite.spritecollide(Pacman, block_list_cherry, True)
      if len(blocks_hit_list_cherry) > 0:
        Blinky.image = pygame.image.load("images/Inky.png").convert()
        Pinky.image = pygame.image.load("images/Inky.png").convert()
        Inky.image = pygame.image.load("images/Inky.png").convert()
        Clyde.image = pygame.image.load("images/Inky.png").convert()
        CAN_KILL = True
      if len(blocks_hit_list) > 0:
          score +=len(blocks_hit_list)     
      screen.fill(black)        
      wall_list.draw(screen)
      gate.draw(screen)
      all_sprites_list.draw(screen)
      monsta_list.draw(screen)
      global life
      text=font1.render("Score: "+str(score)+"/"+str(bll), True, red)
      screen.blit(text, [10, 10])
      text=font1.render("Life: "+str(life), True, red)
      screen.blit(text, [550, 10])
      if score == bll or killed_ghosts == 4:
        doNext("Congratulations, you won!",145,all_sprites_list,block_list,monsta_list,pacman_collide,wall_list,gate)
      monsta_hit_list = pygame.sprite.spritecollide(Pacman, monsta_list, False)
      if monsta_hit_list:
        if CAN_KILL:
          for x in list(monsta_list):
            collided_monsta = pygame.sprite.spritecollide(Pacman, [x], False)
            if collided_monsta:
              x.rect.left = w - 16 + (killed_ghosts*12)
              x.rect.top = m_h 
              killed_ghosts += 1
              returned = x.changespeed([[0, 0, 0]],False,p_turn,p_steps,pl)
              x_turn = returned[0]
              x_steps = returned[1]
              x.changespeed([[0, 0, 0]], False, x_turn, x_steps, 1)
              x.update(wall_list,False)
              if x.name == 'Blinky':
                blinky_killed = True
              elif x.name == 'Pinky':
                pinky_killed = True
              elif x.name == 'Inky':
                inky_killed = True
              elif x.name == 'Clyde':
                clyde_killed = True
              break
        else:
          if life > 0:
            life -= 1
            doCollide("PacMan got Killed.",235,all_sprites_list,block_list,monsta_list,pacman_collide,wall_list,gate)
          else:
            doNext("Game Over",235,all_sprites_list,block_list,monsta_list,pacman_collide,wall_list,gate)     
      pygame.display.flip()    
      clock.tick(10)
def doCollide(message, left, all_sprites_list, block_list, monsta_list, pacman_collide, wall_list, gate):
  while True:
      for event in pygame.event.get():
        if event.type == pygame.QUIT:
          pygame.quit()
        if event.type == pygame.KEYDOWN:
          if event.key == pygame.K_ESCAPE:
            pygame.quit()
          if event.key == pygame.K_RETURN:
            del all_sprites_list
            del block_list
            del monsta_list
            del pacman_collide
            del wall_list
            del gate
            startGame()
      w = pygame.Surface((400,200))  # the size of your rect
      w.set_alpha(10)                # alpha level
      w.fill((128,128,128))           # this fills the entire surface
      screen.blit(w, (100,200))    # (0,0) are the top-left coordinates
      text1=font.render(message, True, white)
      screen.blit(text1, [left-30, 233])
      text2=font.render("To play again, press ENTER.", True, white)
      screen.blit(text2, [135, 303])
      text3=font.render("To quit, press ESCAPE.", True, white)
      screen.blit(text3, [165, 333])
      pygame.display.flip()
      clock.tick(10)
def doNext(message, left, all_sprites_list, block_list, monsta_list, pacman_collide, wall_list, gate):
  while True:
      for event in pygame.event.get():
        if event.type == pygame.QUIT:
          pygame.quit()
        if event.type == pygame.KEYDOWN:
          if event.key == pygame.K_ESCAPE:
            pygame.quit()
      w = pygame.Surface((400,200))  # the size of your rect
      w.set_alpha(10)                # alpha level
      w.fill((128,128,128))           # this fills the entire surface
      screen.blit(w, (100,200))    # (0,0) are the top-left coordinates
      text1=font.render(message, True, white)
      screen.blit(text1, [left, 233])
      text3=font.render("To quit, press ESCAPE.", True, white)
      screen.blit(text3, [165, 333])
      pygame.display.flip()
      clock.tick(10)
startGame()
pygame.quit()

