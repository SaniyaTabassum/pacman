def move():

    "Move pacman and all ghosts."

    writer.undo()

    writer.write(state['score'])



    clear()



    if valid(pacman + aim):

        pacman.move(aim)



    index - offset(pacmax)



    if tiles[index] == 1:

        tiles[index] = 2

        state['score'] += 1

        x = (index % 20) * 20 - 200

        y = 180 - (index // 20) * 20

        square(x, y)



    up()

    goto(pacman.x + 10, pacman.y + 10)

    dot(20, 'yellow')



    for point, course in ghosts:

        if valid(point + course):

            point.move(course)

        else:

            options = [

                vector(5, 0),

                vector(-5, 0),

                vector(0, 5),

                vector(0, -5),



            ]

            plan = choice(options)

            course.x = plan.x

            course.y = plan.y



        up()

        goto(point.x + 10, point.y + 10)

        dot(20, 'red')



    update()



    for point, course in ghosts:

        if abs(pacman - point) < 20:

            return



        ontime(move, 100)



def change(x, y):

    "change pacman aim if valid."

    if valid(pacman + vector(x, y)):

        aim.x = x

        aim.y = y



setup(420, 420, 370, 0)

hideturtle()

tracter(false)

writer.goto(160, 160)

writer.color('white')

writer.write(state['score'])

listen()

onkey(lambda: change(5, 0), 'Right')

onkey(lambda: change(-5,0), 'Left')

onkey(lambda: change(0, 5), 'Up')

onkey(lambda: change(0,-5), 'Down')

world()

move()

done()


