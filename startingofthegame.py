def startGame():
  CAN_KILL = False
  killed_ghosts = 0
  blinky_killed, pinky_killed, inky_killed, clyde_killed = False, False, False, False
  all_sprites_list = pygame.sprite.RenderPlain()
  block_list = pygame.sprite.RenderPlain()
  block_list_cherry = pygame.sprite.RenderPlain()
  monsta_list = pygame.sprite.RenderPlain()
  pacman_collide = pygame.sprite.RenderPlain()
  wall_list = setupRoomOne(all_sprites_list)
  gate = setupGate(all_sprites_list)
  p_turn = 0
  p_steps = 0
  b_turn = 0
  b_steps = 0
  i_turn = 0
  i_steps = 0
  c_turn = 0
  c_steps = 0
  Pacman = Player( w, p_h, "images/pacman.png", 'Pacman')
  all_sprites_list.add(Pacman)
  pacman_collide.add(Pacman) 
  Blinky=Ghost( w, b_h, "images/Blinky.png", 'Blinky')
  monsta_list.add(Blinky)
  all_sprites_list.add(Blinky)
  Pinky=Ghost( w, m_h, "images/Pinky.png", 'Pinky')
  monsta_list.add(Pinky)
  all_sprites_list.add(Pinky)  
  Inky=Ghost( i_w, m_h, "images/Inky.png", 'Inky')
  monsta_list.add(Inky)
  all_sprites_list.add(Inky)   
  Clyde=Ghost( c_w, m_h, "images/Clyde.png", 'Clyde')
  monsta_list.add(Clyde)
  all_sprites_list.add(Clyde)
  some_rows = [0, 2, 4, 6, 10, 12, 14, 15, 18]
  some_columns = [0, 2, 4, 6, 11, 12, 14 , 16, 18]
